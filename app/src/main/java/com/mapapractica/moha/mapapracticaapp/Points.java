package com.mapapractica.moha.mapapracticaapp;

/**
 * Created by moha on 15/03/18.
 */

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Points {
    private int id;
    private String city, name;
    private float latitude, longitude;

    public Points(){

    }

    public Points(String city, int id, float latitude, float longitude, String name){
        this.city = city;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public int getId() { return id; }

    public String getCity() { return city; }

    public String getName() { return name; }

    public float getLatitude() { return latitude; }

    public float getLongitude() { return longitude; }
}

