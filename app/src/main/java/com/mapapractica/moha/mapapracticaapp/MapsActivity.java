package com.mapapractica.moha.mapapracticaapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, AdapterView.OnItemSelectedListener, View.OnClickListener {

    private GoogleMap mMap;
    private Spinner cmbTipusMapa;
    private Button btnCentrar, btnCity;
    private ToggleButton btnAnimacio;

    private static final LatLng INS_BOSC_DE_LA_COMA = new LatLng(42.1727, 2.47631);
    private static final LatLng BARCELONA = new LatLng(41.4144, 2.1026);
    private ToggleButton tgButton;

    private EditText txtCity;
    String city;

    List<Points> llistaPOI = new ArrayList<>();

    DatabaseReference databasePOIs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        databasePOIs = FirebaseDatabase.getInstance().getReference("points");


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        configurarGUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        llegirPunts();

    }

    public void llistarPOI(){
        mMap.clear();

        for(Points poi : llistaPOI){
            String ps = poi.getCity();

            if(poi.getCity().equals(city)){
                LatLng poiLatLng = new LatLng(poi.getLatitude(), poi.getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(poiLatLng)
                        .title(poi.getName()));
            }
        }
    }

    public void llegirPunts() {
        Query q = databasePOIs;

        q.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Points poi = dataSnapshot.getValue(Points.class);
                llistaPOI.add(poi);
                LatLng poiLatLng = new LatLng(poi.getLatitude(), poi.getLongitude());

                mMap.addMarker(new MarkerOptions()
                        .position(poiLatLng)
                        .title(poi.getName()));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void configurarGUI() {

        cmbTipusMapa = (Spinner) findViewById(R.id.cmbTipusMapa);
        cmbTipusMapa.setOnItemSelectedListener(this);

        btnCentrar = (Button) findViewById(R.id.btnCentrar);
        btnCentrar.setOnClickListener(this);

        btnCity = (Button) findViewById(R.id.btnCity);
        btnCity.setOnClickListener(this);

        txtCity = (EditText) findViewById(R.id.txtCity);

        btnAnimacio = (ToggleButton) findViewById(R.id.tglBtnAnimacio);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int position, long id) {
        String tipus = (String) parent.getItemAtPosition(position);

        if (tipus.compareTo("Normal") == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if (tipus.compareTo("Híbrid") == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if (tipus.compareTo("Topogràfic") == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        } else if (tipus.compareTo("Satèl·lit") == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCentrar:
                centrar();
                break;
        }
    }

    private void centrar() {
        if (btnAnimacio.isChecked()) {
            mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(INS_BOSC_DE_LA_COMA, 7),
                    2000, null);
        } else {
            // Moure la càmera a les coordendes del punt que ens interessa
            mMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(INS_BOSC_DE_LA_COMA, 7));
        }
    }

    private void chooseCity() {

        city = txtCity.getText().toString();


        for (Points punt : llistaPOI){
            if (city.equals(punt.getCity())) {
                llistarPOI();
                LatLng ubicacio = new LatLng(punt.getLatitude(), punt.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ubicacio, 12), 2000, null);
            }else{
                Toast.makeText(this, "No  s'ha trobat el punt", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override

    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        btnCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseCity();
            }
        });

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(INS_BOSC_DE_LA_COMA, 8));

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mMap.getUiSettings().setCompassEnabled(true);

        mMap.getUiSettings().setZoomControlsEnabled(true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);


//
    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}